#include <base/solution.h>
#include <base/automata.h>

#include <cstdlib>
#include <ctime>
#include <vector>
#include <fstream>
#include <algorithm>

const int maxSize = 30;

Solution initRules(std::vector<int> & modifiableRules){
  int size = 6 ;
  Solution solution(nbStates);
  for (int g = 0; g < size; ++g) {
    for (int c = 0; c < size; ++c) {
      for (int d = 0; d < size; ++d) {
	int pos = g * 36 + c * 6 + d; 
	//regles qui ne seront jamais utilisés
	if (g == 4 or c == 4 or d == 4 or c == 5 or (g == 5 and d == 5)) {
	  solution.rules [pos] = 5;
	  continue;
	}
	// regles pour 000, 00B et B00
	if (pos == 0 or pos == 5 or pos == 180) {
	  solution.rules [pos] = 0;
	  continue;
	}
	//111, 11B et B11
	if (pos == 43 or pos == 47 or pos == 187) {
	  solution.rules [pos] = 4;
	  continue;
	}
	//10B et B01
	if (pos == 41 or pos == 186) {
	  solution.rules [pos] = 1;
	  continue;
	}
	//sinon, on indique que la regle est modifiable
	modifiableRules.push_back(pos);
      }
    }
  }
  return solution;
}

void generateRandomRules(Solution & solution,
			 const std::vector<int> & modifiableRules) {
  for (int i = 0; i < modifiableRules.size(); ++i) {
    solution.rules[modifiableRules[i]] = rand() % 4;
  }
}

//genere un vecteur dont les elements sont de la forme
//{Indice de la regle a modifier, valeur du decalage}
std::vector<std::vector<int>> generatePermutation(int rulesNumber) {
  std::vector<std::vector<int>> permutation;
  permutation.reserve(rulesNumber * 3);
  for(int i = 0; i < rulesNumber; ++i) {
    for(int j = 1; j < 4; ++j) {
      std::vector<int> modification = {i, j};
      permutation.push_back(modification);
    }
  }
  return permutation;
}

Solution perturbation(const Solution & initialSolution,
		      const std::vector<int> & modifiableRules,
		      int perturbationSize) {
  Solution s(initialSolution);
  for(int i = 0; i < perturbationSize; ++i) {
    s.rules[modifiableRules[rand() % modifiableRules.size()]] =
      rand() % 4;
  }
  return s;
}

int hillClimber(int nbEvalMaxHC, std::vector<std::vector<int>> & permutation,
		     Solution & s,
		     std::vector<int> & modifiableRules, Automata & automate)
{
  bool optimum = false;
  automate.eval(s, maxSize);
  int nbEval = 1;
  while(nbEval < nbEvalMaxHC and !optimum) {
    optimum = true;
    //on genere un ordre de parcours aleatoire des voisins
    std::random_shuffle(permutation.begin(), permutation.end());
    Solution testedSolution(s);
    int i = 0;
    while(i < permutation.size() and nbEval < nbEvalMaxHC and optimum) {
      //on modifie s de sorte a qu'il corresponde a un voisin de la meilleur solution
      //puis on l'evalue et le compare a la meilleure solution
      int ruleToModify = modifiableRules[permutation[i][0]];
      int ruleValue = testedSolution.rules[ruleToModify];
      testedSolution.rules[ruleToModify] = (ruleValue + permutation[i][1]) % 4;
      automate.eval(testedSolution, maxSize);
      ++nbEval;
      if(testedSolution.fitness() >= s.fitness()) {
	s = testedSolution;
	optimum = false;
      }
      //on remet la solution testée à sa valeur d'origine si elle est moins bonne que la meilleur solution
      testedSolution.rules[ruleToModify] = ruleValue;
      ++i;
    }
  }
  return nbEval;
}

Solution iteratedLocalSearchHC(int nbEvalMax, int nbEvalMaxHc,
			       int perturbationSize, Automata & automate) {
  
  std::vector<int> modifiableRules;
  Solution bestSolution = initRules(modifiableRules);
  generateRandomRules(bestSolution, modifiableRules);
  std::vector<std::vector<int>> permutation =
    generatePermutation(modifiableRules.size());
  int nbEval = hillClimber(nbEvalMaxHc, permutation, bestSolution,
			   modifiableRules, automate);
  
  std::fstream fileout("../solution/solution_" + std::to_string(bestSolution.fitness()) + ".dat", std::ios::out);
  fileout << bestSolution << std::endl;
  fileout.close();

  while(nbEval < nbEvalMax) {
    Solution s = perturbation(bestSolution, modifiableRules, perturbationSize);
    nbEval += hillClimber(nbEvalMaxHc, permutation, s,
		modifiableRules, automate);
    if(s.fitness() > bestSolution.fitness()) {
      bestSolution = s;
      std::fstream fileout("../solution/solution_" + std::to_string(bestSolution.fitness()) + ".dat", std::ios::out);
      fileout << bestSolution << std::endl;
      fileout.close();
    }
  }
  return bestSolution;
}


int main(int argc, char ** argv) {
  if(argc != 4) {
    std::cout << "usage : " << argv[0] << " <nbEvalTotal> <nbEvalHc> <perturbationSize>";
    return 0;
  }
  srand(std::time(NULL));
  Automata automate(maxSize);
  Solution s = iteratedLocalSearchHC(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), automate);
  std::cout << s.fitness() << std::endl;
  automate.exportSVG(s, s.fitness(), "../svg/ILS.svg", true);
  return 0;
}
