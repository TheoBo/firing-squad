How to compile :

cd firing-squad
mkdir build
cd build
cmake ..
make

use with ./search <total number of evaluations> <max number of evaluations per hill climber> <Size of the perturbation>
Results can be found in the solution and svg directories and the fitness of the best solution will be indicated in the terminal


ResultsNbEvalsHc.dat and ResultsPerturbationSize.dat contain the data which was used to analyse performance in function of the parameters
